# tensorflow-example
Example DNN classifier used to demonstrate TensorFlow in a Scott Logic blog post.

<a href="https://archive.ics.uci.edu/ml/datasets/semeion+handwritten+digit">Training and test data downloaded from UCI</a>.

## Python Compatibility
At present, TensorFlow is not compatible with Python 3.7. See [TensorFlow issue 20517](https://github.com/tensorflow/tensorflow/issues/20517). In the meantime, please use a 64-bit version of either Python 3.5 or Python 3.6.